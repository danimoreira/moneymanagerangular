export class HomeItem {
    image: string;
    title: string;
    description: string;
}