import { HomeItem } from "./homeItem";

export const HOMEITEMS: HomeItem[] = [
    {
        image: "/assets/images/test.png",
        title: "Title of the item",
        description: "Small description about the related item with image"
    },
    {
        image: "/assets/images/test.png",
        title: "Title of the item",
        description: "Small description about the related item with image"
    },
    {
        image: "/assets/images/test.png",
        title: "Title of the item",
        description: "Small description about the related item with image"
    }
]