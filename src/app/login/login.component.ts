import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  // variable used to retain the password field hiding status
  private hide: boolean = true;

  constructor() { }

  ngOnInit() {
  }

}
