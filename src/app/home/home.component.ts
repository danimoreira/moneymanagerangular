import { Component, OnInit } from '@angular/core';
import { HomeService } from '../services/home.service';
import { HomeItem } from '../shared/homeItem';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  private homeItems: HomeItem[];

  constructor(private homeService: HomeService) { }

  ngOnInit() {
    this.homeItems = this.homeService.getHomeItems();
  }

}
