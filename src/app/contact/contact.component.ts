import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  private subjects: string[] = [
    "Feedback about the application",
    "Troubleshooting and errors",
    "Professional contact / Job proposition",
    "Other (please specify in the comment"
  ];

  constructor() { }

  ngOnInit() {
  }

}
