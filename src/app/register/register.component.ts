import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  private hide: boolean = true;

  private usernameInput = new FormControl('', [Validators.required]);
  private firstnameInput = new FormControl('', [Validators.required]);
  private lastnameInput = new FormControl('', [Validators.required]);

  constructor() { }

  ngOnInit() {
  }

  public areFieldsEmpty(): boolean {
    return this.usernameInput.hasError('required') &&
            this.firstnameInput.hasError('required') &&
            this.lastnameInput.hasError('required');
  }
}
