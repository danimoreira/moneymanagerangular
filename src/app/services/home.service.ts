import { Injectable } from '@angular/core';

import { HomeItem } from '../shared/homeItem';
import { HOMEITEMS } from "../shared/homeItems";

@Injectable()
export class HomeService {

  constructor() { }

  public getHomeItems(): HomeItem[] {
    return HOMEITEMS;
  }

}
